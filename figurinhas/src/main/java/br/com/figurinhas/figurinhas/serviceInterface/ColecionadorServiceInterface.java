package br.com.figurinhas.figurinhas.serviceInterface;

import java.util.List;

import br.com.figurinhas.figurinhas.model.Colecionador;

public interface ColecionadorServiceInterface {

	public List<Colecionador> buscarColecionadores();
	
	public Colecionador pesquisarColecionador(long id);
	
	public Colecionador cadastrarColecionador(Colecionador colecionador);
	
	public void excluirColecionador(long id);
	
	public Colecionador atualizarColecionador(Colecionador colecionador);

}
