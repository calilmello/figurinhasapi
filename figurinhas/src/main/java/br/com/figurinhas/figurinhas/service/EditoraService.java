package br.com.figurinhas.figurinhas.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.figurinhas.figurinhas.model.Editora;
import br.com.figurinhas.figurinhas.repository.EditoraRepository;
import br.com.figurinhas.figurinhas.serviceInterface.EditoraServiceInterface;

@Service
public class EditoraService implements EditoraServiceInterface{

	@Autowired
	EditoraRepository repositorio;
	
	@Override
	public List<Editora> buscarEditoras() {
		return repositorio.findAll();
	}

	@Override
	public Editora pesquisarEditora(long id) {
		return repositorio.findById(id);
	}

	@Override
	public Editora cadastrarEditora(Editora editora) {
		return repositorio.save(editora);
	}

	@Override
	public void excluirEditora(long id) {
		repositorio.deleteById(id);
	}

	@Override
	public Editora atualizarEditora(Editora editora) {
		return repositorio.save(editora);
	}


}
