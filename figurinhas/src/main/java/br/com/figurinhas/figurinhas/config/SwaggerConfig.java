//
//package br.com.figurinhas.figurinhas.config;
//
//import java.util.ArrayList;
//
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
//import com.google.common.base.Predicate;
//
//import springfox.documentation.builders.RequestHandlerSelectors;
//import springfox.documentation.service.ApiInfo;
//import springfox.documentation.service.Contact;
//import springfox.documentation.service.VendorExtension;
//import springfox.documentation.spi.DocumentationType;
//import springfox.documentation.spring.web.plugins.Docket;
//import springfox.documentation.swagger2.annotations.EnableSwagger2;
//
//@Configuration
//@EnableSwagger2
//public class SwaggerConfig {
//
//	@Bean
//	public Docket figurinhasAPI() {
//		return new Docket(DocumentationType.SWAGGER_2)
//				.select()
//				.apis(RequestHandlerSelectors.basePackage("br.com.figurinhas.figurinhas"))
//				.paths(regex("/figurinhas.*"))
//				.build()
//				.apiInfo(metaInfo());
//	}
//
//	private Predicate<String> regex(String string) {
//		return null;
//	}
//
//	private ApiInfo metaInfo() { 
//		ApiInfo apiInfo = new ApiInfo(
//				 "Figurinhas Figurinhas", "API REST de cadastro de figurinhas", 
//				 "1.0",
//				  "Tems of Service", 
//				  new Contact("Calil Mello", "calilmello@gmail.com", null),
//				  "Apache License Version 2.0", 
//				  "http://www.apche.org/lecensen.html", 
//				  new ArrayList<VendorExtension>()
//		);
//		return apiInfo;
//	}
//}
