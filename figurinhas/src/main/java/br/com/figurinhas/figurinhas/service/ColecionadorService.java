package br.com.figurinhas.figurinhas.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.figurinhas.figurinhas.model.Colecionador;
import br.com.figurinhas.figurinhas.repository.ColecionadorRepository;
import br.com.figurinhas.figurinhas.serviceInterface.ColecionadorServiceInterface;

@Service
public class ColecionadorService implements ColecionadorServiceInterface{

	@Autowired
	ColecionadorRepository repositorio;
	
	@Override
	public List<Colecionador> buscarColecionadores() {
		return repositorio.findAll();
	}

	@Override
	public Colecionador pesquisarColecionador(long id) {
		return repositorio.findById(id);
	}

	@Override
	public Colecionador cadastrarColecionador(Colecionador colecionador) {
		return repositorio.save(colecionador);
	}

	@Override
	public void excluirColecionador(long id) {
		repositorio.deleteById(id);
	}

	@Override
	public Colecionador atualizarColecionador(Colecionador colecionador) {
		return repositorio.save(colecionador);
	}

}
