package br.com.figurinhas.figurinhas.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.figurinhas.figurinhas.model.Colecao;
import br.com.figurinhas.figurinhas.model.Colecionador;
import br.com.figurinhas.figurinhas.repository.ColecaoRepository;
import br.com.figurinhas.figurinhas.repository.ColecionadorRepository;
import br.com.figurinhas.figurinhas.repository.EditoraRepository;
import br.com.figurinhas.figurinhas.serviceInterface.ColecaoServiceInterface;
import br.com.figurinhas.figurinhas.serviceInterface.ColecionadorServiceInterface;
import br.com.figurinhas.figurinhas.serviceInterface.EditoraServiceInterface;

@Service
public class ColecaoService implements ColecaoServiceInterface{

	@Autowired
	ColecaoRepository repositorio;
	
	@Override
	public List<Colecao> buscarColecao() {
		return repositorio.findAll();
	}

	@Override
	public Colecao pesquisarColecao(long id) {
		return repositorio.findById(id);
	}

	@Override
	public Colecao cadastrarColecao(Colecao colecao) {
		return repositorio.save(colecao);
	}

	@Override
	public void excluirColecao(long id) {
		repositorio.deleteById(id);
	}

	@Override
	public Colecao atualizarColecao(Colecao colecao) {
		return repositorio.save(colecao);
	}

	
}
