package br.com.figurinhas.figurinhas.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.figurinhas.figurinhas.model.Editora;
import br.com.figurinhas.figurinhas.service.EditoraService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value="/figurinhas")
@Api(value="Figurinhas")
@CrossOrigin(origins="*")
public class EditoraController {

	@Autowired
	EditoraService servico;
	
	@GetMapping("/editora")
	@ApiOperation(value = "Retorna uma lista de albuns")
	public List<Editora> listaEditora() {
		return servico.buscarEditoras();
	}
	
	@GetMapping("/editora/{id}")
	@ApiOperation(value = "Retorna uma editora")
	public Editora listaEditoraEspecifica(@PathVariable(value="id")long id) {
		return servico.pesquisarEditora(id);
	}
	
	@PostMapping("/editora")
	@ApiOperation(value = "Insere dados em editora")
	public Editora salvaEditora(@RequestBody Editora editora) {
		return servico.cadastrarEditora(editora);
	}
		
	@DeleteMapping("/editora/{id}")
	@ApiOperation(value = "Deleta itens em editora")
	public String deletaEditora(@PathVariable(value="id")long id) {
		servico.excluirEditora(id);
		return "Editora excluída com sucesso";
	}
	
	@PutMapping("/editora") 
	@ApiOperation(value = "Atualiza editora")
	public Editora atualizaEditora(@RequestBody Editora editora) {
	  return servico.atualizarEditora(editora);
	 
	 }
}
