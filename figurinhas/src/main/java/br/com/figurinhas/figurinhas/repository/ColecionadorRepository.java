package br.com.figurinhas.figurinhas.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.figurinhas.figurinhas.model.Colecionador;

public interface ColecionadorRepository  extends JpaRepository<Colecionador, Long>{
	
	Colecionador findById(long id);

}
