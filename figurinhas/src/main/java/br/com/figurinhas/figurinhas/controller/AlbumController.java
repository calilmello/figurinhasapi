package br.com.figurinhas.figurinhas.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.figurinhas.figurinhas.model.Album;
import br.com.figurinhas.figurinhas.repository.AlbumRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value="/figurinhas")
@Api(value="Figurinhas")
@CrossOrigin(origins="*")
public class AlbumController {

	@Autowired
	AlbumRepository albumRepository;
	
	@GetMapping("/album")
	@ApiOperation(value = "Retorna uma lista de albuns")
	public List<Album> listaAlbum() {
		return albumRepository.findAll();
	}
	
	@GetMapping("/album/{id}")
	@ApiOperation(value = "Retorna um album")
	public Album listaAlbumEspecifico(@PathVariable(value="id")long id) {
		return albumRepository.findById(id);
	}
	
	@PostMapping("/album")
	@ApiOperation(value = "Insere dados em albuns")
	public Album salvaAlbum(@RequestBody Album album) {
		return albumRepository.save(album);
	}
		
	@DeleteMapping("/album/{id}")
	@ApiOperation(value = "Deleta itens em albuns")
	public void deletaAlbum(@PathVariable(value="id")long id) {
		albumRepository.deleteById(id);
	
	}
	
	@PutMapping("/album") 
	@ApiOperation(value = "Atualiza albuns")
	public Album atualizaAlbum(@RequestBody Album album) {
	  return albumRepository.save(album);
	 
	 }
	}
