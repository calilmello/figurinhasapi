package br.com.figurinhas.figurinhas.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.figurinhas.figurinhas.model.Album;

public interface AlbumRepository  extends JpaRepository<Album, Long>{
	
	Album findById(long id);

}
