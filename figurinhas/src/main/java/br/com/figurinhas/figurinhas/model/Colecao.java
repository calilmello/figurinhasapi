package br.com.figurinhas.figurinhas.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name="COLECAO",uniqueConstraints = 
			{@UniqueConstraint(columnNames= {})})
public class Colecao implements Serializable {
	
	private static final long serialVersionUID = 1l;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	
	private long id;
	
	public long getId() {
		return id;
	}
	
	@OneToOne
	private Colecionador colecionador;
	
	@OneToOne
	private Album album;
	
	private String figurinhas;
	
	
	public Colecionador getColecionador() {
		return colecionador;
	}
	public void setColecionador(Colecionador colecionador) {
		this.colecionador = colecionador;
	}
	public Album getAlbum() {
		return album;
	}
	public void setAlbum(Album album) {
		this.album = album;
	}
	public String getFigurinhas() {
		return figurinhas;
	}
	public void setFigurinhas(String figurinhas) {
		this.figurinhas = figurinhas;
		
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public void setId(long id) {
		this.id = id;
	}
	
}
