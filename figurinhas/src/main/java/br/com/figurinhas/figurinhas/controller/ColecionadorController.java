package br.com.figurinhas.figurinhas.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.figurinhas.figurinhas.model.Colecionador;
import br.com.figurinhas.figurinhas.service.ColecionadorService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value="/figurinhas")
@Api(value="Figurinhas")
@CrossOrigin(origins="*")
public class ColecionadorController {

	@Autowired
	ColecionadorService servico;
	
	@GetMapping("/colecionador")
	@ApiOperation(value = "Retorna uma lista de colecionador")
	public List<Colecionador> listaColecionador() {
		return servico.buscarColecionadores();
	}
	
	@GetMapping("/colecionador/{id}")
	@ApiOperation(value = "Retorna um colecionador")
	public Colecionador listaColecionadorEspecifico(@PathVariable(value="id")long id) {
		return servico.pesquisarColecionador(id);
	}
	
	@PostMapping("/colecionador")
	@ApiOperation(value = "Insere um colecionador")
	public Colecionador salvaColecionador(@RequestBody Colecionador colecionador) {
		return servico.cadastrarColecionador(colecionador);
	}
		
	@DeleteMapping("/colecionador/{id}")
	@ApiOperation(value = "Deleta um colecionador")
	public String deletaColecionador(@PathVariable(value="id")long id) {
		servico.excluirColecionador(id);
		return "Colecionador excluído com sucesso";
	
	}
	
	@PutMapping("/colecionador") 
	@ApiOperation(value = "Atualiza colecionador")
	public Colecionador atualizaColecionador(@RequestBody Colecionador colecionador) {
	  return servico.atualizarColecionador(colecionador);
	 
	}

}
