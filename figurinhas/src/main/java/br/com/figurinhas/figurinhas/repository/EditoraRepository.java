package br.com.figurinhas.figurinhas.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.figurinhas.figurinhas.model.Editora;

public interface EditoraRepository  extends JpaRepository<Editora, Long>{
	
	Editora findById(long id);

}
