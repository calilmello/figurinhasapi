package br.com.figurinhas.figurinhas.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.figurinhas.figurinhas.model.Colecao;
import br.com.figurinhas.figurinhas.model.Colecionador;
import br.com.figurinhas.figurinhas.repository.ColecaoRepository;
import br.com.figurinhas.figurinhas.repository.ColecionadorRepository;
import br.com.figurinhas.figurinhas.service.ColecaoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value="/figurinhas")
@Api(value="Figurinhas")
@CrossOrigin(origins="*")
public class ColecaoController {

	@Autowired
	ColecaoService servico;
	
	@GetMapping("/colecao")
	@ApiOperation(value = "Retorna uma lista de colecao")
	public List<Colecao> listaColecao() {
		return servico.buscarColecao();
	}
	
	@GetMapping("/colecao/{id}")
	@ApiOperation(value = "Retorna uma colecao")
	public Colecao listaColecaoEspecifica(@PathVariable(value="id")long id) {
		return servico.pesquisarColecao(id);
	}
	
	@PostMapping("/colecao")
	@ApiOperation(value = "Insere um colecao")
	public Colecao salvaColecao(@RequestBody Colecao colecao) {
		return servico.cadastrarColecao(colecao);
	}
		
	@DeleteMapping("/colecao/{id}")
	@ApiOperation(value = "Deleta um colecao")
	public String deletaColecao(@PathVariable(value="id")long id) {
		servico.excluirColecao(id);
		return "Colecao excluída com sucesso";
	}
	
	@PutMapping("/colecao") 
	@ApiOperation(value = "Atualiza colecao")
	public Colecao atualizaColecao(@RequestBody Colecao colecao) {
	  return servico.atualizarColecao(colecao);
	}

}
