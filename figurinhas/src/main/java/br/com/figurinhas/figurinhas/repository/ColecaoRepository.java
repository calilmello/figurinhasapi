package br.com.figurinhas.figurinhas.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.figurinhas.figurinhas.model.Colecao;

public interface ColecaoRepository  extends JpaRepository<Colecao, Long>{
	
	Colecao findById(long id);

}
