package br.com.figurinhas.figurinhas.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name="ALBUM",uniqueConstraints = 
	{@UniqueConstraint(columnNames= {"nome"})})

public class Album implements Serializable {
	
	private static final long serialVersionUID = 1l;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	
	private String nome;
	private int ano;
	@OneToOne
	private Editora editora;
	private int quantidadeFigurinhas;
	private String imagem;
	
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public int getAno() {
		return ano;
	}
	public void setAno(int ano) {
		this.ano = ano;
	}
	public Editora getEditora() {
		return editora;
	}
	public void setEditora(Editora editora) {
		this.editora = editora;
	}
	public int getQuantidadeFigurinhas() {
		return quantidadeFigurinhas;
	}
	public void setQuantidadeFigurinhas(int quantidadeFigurinhas) {
		this.quantidadeFigurinhas = quantidadeFigurinhas;
	}
	public String getImagem() {
		return imagem;
	}
	public void setImagem(String imagem) {
		this.imagem = imagem;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
