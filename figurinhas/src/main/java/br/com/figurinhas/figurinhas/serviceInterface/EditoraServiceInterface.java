package br.com.figurinhas.figurinhas.serviceInterface;

import java.util.List;

import br.com.figurinhas.figurinhas.model.Editora;

public interface EditoraServiceInterface {

	public List<Editora> buscarEditoras();
	
	public Editora pesquisarEditora(long id);
	
	public Editora cadastrarEditora(Editora editora);
	
	public void excluirEditora(long id);
	
	public Editora atualizarEditora(Editora editora);
}
