package br.com.figurinhas.figurinhas.serviceInterface;

import java.util.List;

import br.com.figurinhas.figurinhas.model.Colecao;

public interface ColecaoServiceInterface {

	public List<Colecao> buscarColecao();
	
	public Colecao pesquisarColecao(long id);
	
	public Colecao cadastrarColecao(Colecao colecao);
	
	public void excluirColecao(long id);
	
	public Colecao atualizarColecao(Colecao colecao);

}
